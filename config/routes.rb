Rails.application.routes.draw do

  devise_scope :user do
     get 'logout', to: 'devise/sessions#destroy'
   end
  
  devise_for :users, 
              path_names: { :sign_in=>'login', sign_up: 'register'},
              :controllers => {:registrations => "api/devise/registrations",:sessions=>"api/devise/sessions",:passwords => 'api/devise/passwords'} 

  namespace :api do
    resources :users do 
      collection do 
        get "get_user"
        get "fetch_basic_data" 
       end 
    end  

    resources :transactions do 
      collection do 
        
       end 
    end 

    resources :groups  
  end   

  get '/:path' => 'home#index' , as: 'react_path'

  root to: "home#index"
end
