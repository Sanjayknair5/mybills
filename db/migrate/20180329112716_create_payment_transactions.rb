class CreatePaymentTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_transactions do |t|
      t.integer :amount, :limit => 8
      t.string :mode
      t.references :user
      t.string :recipient_type
      t.integer :recipient_id
      t.string :split_mode
      t.string :pay_mode
      t.string :name 
      t.timestamps
    end
    add_index :payment_transactions, [:recipient_type,:recipient_id]
  end
end
