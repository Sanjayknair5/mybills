class CreateFriendLedgers < ActiveRecord::Migration[5.1]
  def change
    create_table :friend_ledgers do |t|
      t.integer :u1_id
      t.integer :u2_id
      t.integer :balance, :limit => 8
      t.timestamps
    end
    add_index :friend_ledgers, :u1_id
    add_index :friend_ledgers, :u2_id
  end
end
