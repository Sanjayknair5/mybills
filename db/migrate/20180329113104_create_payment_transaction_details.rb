class CreatePaymentTransactionDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :payment_transaction_details do |t|
      t.references :payment_transaction
      t.references :user
      t.integer :payed  , :limit => 8
      t.integer :share , :limit => 8
      t.timestamps
    end
  end
end
