class CreateMasterLedgers < ActiveRecord::Migration[5.1]
  def change
    create_table :master_ledgers do |t|
      t.references :user
      t.integer :payable , :limit => 8
      t.integer :receivable , :limit => 8
      t.timestamps
    end
  end
end
