# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if User.count == 0
   [
      ["sanjayknair5@gmail.com","sanjay123","sanjay k"],
      ["rishi@unbxd.com","rishi123",'Rishi'],
      ["navaneeth@unbxd.com","navaneeth123","Navaneeth123"],
      ["arun@unbxd.com","arun123","Arun"],
      ["nithin@unbxd.com","nithin123","Nithin"]
   ].each do |ua|
      user = User.new do |u|
         u.email = ua[0]
         u.password = ua[1]
         u.name = ua[2]
      end
      if user.save
       puts "user created with login #{user.email}"
      else 
       puts user.errors.full_messages  
      end  
   end
end