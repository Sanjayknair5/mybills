require "rails_helper"

RSpec.describe PaymentTransaction, :type => :model do

  before :example , :create_user => true do
     [
      ["sanjayknair5@gmail.com","sanjay123","sanjay k"],
      ["rishi@unbxd.com","rishi123",'Rishi']
     ].each do |ua|
      user = User.new do |u|
         u.email = ua[0]
         u.password = ua[1]
         u.name = ua[2]
      end
      user.save
     end 
  end


  context "valid input should create transaction" do
    it "create transaction and update ledgers" ,:create_user => true do
      user = User.first
      resp = User.last
      params = {name: "test", amount: 500, mode: "paid_by_me_and_split_equally",recipient_id:"#{resp.id}_user"}
      transaction = PaymentTransaction.check_and_create(params,user)
      expect(transaction.payment_transaction_details.count).to eq 2
      expect(transaction.errors.count).to eq 0
      expect(user.master_ledger.payable).to eq 250
      expect(user.master_ledger.receivable).to eq 500
      expect(resp.master_ledger.receivable).to eq 0
      expect(resp.master_ledger.payable).to eq 250
    end
  end
end