class Api::TransactionsController < ApplicationController
  before_action  :authenticate_user! 
  before_action  :set_user


  def index
    transactions = @user.payment_transaction_details.includes(:payment_transaction =>[:user]).order("created_at desc")
    trans_ary = []
    transactions.each do |trans|
      master_trans = trans.payment_transaction
      msg = "#{master_trans.user.name} recorded payment of #{master_trans.amount} for #{master_trans.name} on #{master_trans.created_at.strftime('%d %b')}."
      if trans.payed > trans.share
       msg += " You get #{trans.payed - trans.share}"
      else
       msg += " You owe #{trans.share - trans.payed}" 
      end  
      trans_ary << msg
    end  
    render json: {transactions: trans_ary}
  end  

  def new
    partners = User.where("id != #{@user.id}")
    groups = @user.groups
    render json: {partners: partners, groups: groups}
  end


  def create
    payment_transaction = PaymentTransaction.check_and_create(transaction_params,@user)
    if payment_transaction.errors.blank?
      render json: {status: "success", id: payment_transaction.id}
    else
      render json: {status: "fail", errors: payment_transaction.errors.full_messages}
    end
  end  


  private

  def set_user
   @user = current_user
  end

  def transaction_params
      params.require(:transaction).permit(:amount,:recipient_id,:name, :mode)
  end
end
