class Api::GroupsController < ApplicationController
  before_action  :authenticate_user! 
  before_action  :set_user


 def index
  groups = @user.groups
  render json: groups
 end  




 def create
   group = Group.new(group_params)
   group.user_ids = group.user_ids + [@user.id]
   if group.save 
   render json: {status: "success", id: group.id}
   else
    render json: {status: "error", errors: group.errors.full_messages}
   end
 end 


  


 private

  def set_user
    @user = current_user
  end

  def group_params
    params.require(:group).permit(:name,:user_ids=>[])
  end

end
