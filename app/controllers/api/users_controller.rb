class Api::UsersController < ApplicationController

  before_action  :authenticate_user! , except:[:get_user]
  before_action  :set_user

  def get_user
    if current_user.present?
      render json: {status: true, name: "#{current_user.name}"}
    else
      render json: {status: false}
    end
  end




  def fetch_basic_data
    ledger = @user.master_ledger

    if ledger.present?
      balance = (ledger.receivable - ledger.payable)

      if balance == 0
        status_msg = "All Transactions Settled"
        display_bal = nil
      elsif balance > 0
        status_msg = "You should get"
        display_bal = balance
      else
        status_msg = "You owes"
        display_bal = balance.abs
      end
      render json: {payable: ledger.payable, receivable: ledger.receivable, balance: display_bal, status_msg: status_msg}
    else
      render json: {payable: 0, receivable: 0, balance: "", status_msg: "No Transactions"}  
    end
  end



  private


  def set_user
    @user = current_user
  end

end
