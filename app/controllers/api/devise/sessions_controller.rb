class Api::Devise::SessionsController < Devise::SessionsController


  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message!(:notice, :signed_in)
    sign_in(resource_name, resource)
    render json: {status: "success"} and return
  end

end