import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField';
import { withStyles } from 'material-ui/styles';
import axios from 'axios';
import { FormControlLabel, FormGroup } from 'material-ui/Form';
import { BrowserRouter as Router, Route, Link , Redirect} from 'react-router-dom';



class Login extends React.Component {


constructor(props) {
    super(props);
    this.state = {username: '',password: ""};
    this.handleUsername = this.handleUsername.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
    this.handleClick = this.handleClick.bind(this);
}


handleUsername(event) {
  this.setState({username: event.target.value});
}

handlePassword(event) {
  this.setState({password: event.target.value});
}


handleClick = (event) => {
  if (this.state.username &&  this.state.password){
     var apiBaseUrl = "/users/";
     var self = this;
     var payload={
     "email":this.state.username,
     "password":this.state.password
     }
     axios.post(apiBaseUrl+'login', {"user": payload})
     .then(function (response) {
     if(response.status == 200){
     console.log("Login successfull");
      window.location.href = "/";
     }
     else{
     alert("Error Occured");
     }
     })
     .catch(function (error) {
       console.log(error);
       alert("Login error. Check your credentials");
     });
  }else{
    alert("Please will all fields");
  }
}



render() {

      const { classes } = this.props;
      const { from } =  '/login'
 
       return ( 
          <div className={classes.mainCont}>
          <h1>Login</h1>
           <FormGroup>
           <TextField
             value={this.state.username} 
             onChange = {this.handleUsername}
             label="Email"
             />
           <br/>
             <TextField
               type="password"
               value={this.state.password} 
               onChange = {this.handlePassword}
               label="Password"
               />
             <br/>
             <Button variant="raised" color="primary" onClick={(event) => this.handleClick(event)}>
             Login
             </Button>

            </FormGroup> 
         </div>


         )
        }
 } 

// Login class ends


Login.defaultProps = {
  username: '',
  password: ''
}

const styles = {
 mainCont: {
   width: '650px',
   margin: '0 auto'
  },
};

Login.propTypes = {
  username: PropTypes.string,
  password: PropTypes.string,
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Login);



