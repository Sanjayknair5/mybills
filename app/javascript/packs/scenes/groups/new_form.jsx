import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import axios from 'axios';
import { BrowserRouter as Router, Route, Link , Redirect} from 'react-router-dom';
import Input, { InputLabel, InputAdornment} from 'material-ui/Input';
import { FormControl, FormHelperText, FormControlLabel} from 'material-ui/Form';
import TextField from 'material-ui/TextField';
import Icon from 'material-ui/Icon';
import Save from 'material-ui-icons/Save';
import Button from 'material-ui/Button';
import classNames from 'classnames';
import Checkbox from 'material-ui/Checkbox';


const styles = theme => ({
  root: {
    width: '500px',
    margin: '0 auto',
  },

  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    display:'flex',
  },
  button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});


class NewForm extends React.Component {


  constructor(props){
    super();
    this.state = {
      partners: props.partners,
      recipient_id: '',
      amount: "",
      fireRedirect: false,
      selectedCheckboxes: []
      
    }
    this.handleClick = this.handleClick.bind(this);
    this.checkToken = this.checkToken.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    this.handleChange = this.handleChange.bind(this);
  };

  componentWillMount(){
    console.log("add ts")
    console.log(this.state.partners.length)
    this.state.selectedCheckboxes = [];
  };


  handleClick = (event) => {
   
   if (this.state.selectedCheckboxes.length > 0 &&  this.state.name){
     var apiBaseUrl = "/api/";
     var self = this;
     var payload={
     "user_ids":this.state.selectedCheckboxes,
     "name":this.state.name
     }
     axios.post(apiBaseUrl+'groups', {"group": payload})
     .then(function (response) {
     console.log(response);
     if(response.data.status == "success"){
     console.log("Group Created successfully");
     self.setState({ fireRedirect: true })
     }else if(response.status == 401){
     console.log("Invalid Input");
     }
     else{
     console.log("Error Occured");
     }
     })
     .catch(function (error) {
       console.log(error);
     });
  }else{
    alert("Please will all fields");
  }   
 };

 handleChange = event => {
    this.setState({ name: event.target.value });
  };


  handleCheck = prop => event => {
    const ar = this.state.selectedCheckboxes 
    if (ar.includes(prop)) {
       var ind = ar.indexOf(prop)
       ar.splice(ind, 1);
    } else {
       ar.push(prop);

    }
    this.setState({ selectedCheckboxes: ar })
  };


checkToken = (prop) =>{
  console.log(prop);
  if (this.state.selectedCheckboxes.includes(prop)) {
    return "checked"
  }else{
     return ""  
  }
}


  render() {

    const { classes } = this.props;
    const { from } =  '/add_group';
    const { fireRedirect, partners, selectedCheckboxes} = this.state;
    let options = [];
    for (let i=0; i < partners.length ; i += 1) { options.push([partners[i].name,partners[i].id]); }
    return (
      <div className={classes.root}>
        <h2> Add Group </h2>
        <form className={classes.root} autoComplete="off">
          <FormControl className={classes.formControl}>
            <p>Select Group Members</p>  
              {options.map((i) => {
                return (
                        <FormControlLabel
                        control={
                        <Checkbox
                        checked={selectedCheckboxes.includes(i[1])}
                        onChange={this.handleCheck(i[1])}
                        value={i[1]}
                         />
                        }
                        label={i[0]} 
                        /> 
                      )
              })}
            </FormControl>
            <FormControl className={classes.formControl}>
              <InputLabel htmlFor="name-simple">Name</InputLabel>
              <Input id="name-simple" value={this.state.name} onChange={this.handleChange} />
            </FormControl>

            <Button className={classes.button} variant="raised" size="small" onClick={(event) => this.handleClick(event)} >
            <Save className={classNames(classes.leftIcon, classes.iconSmall)} />
            Save
          </Button>
        </form>

        {fireRedirect && (
          <Redirect to={from || '/groups'}/>
        )}
      </div>
    );
  }
}

NewForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NewForm);
