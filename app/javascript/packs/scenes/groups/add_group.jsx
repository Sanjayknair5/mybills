import React, { Component } from 'react';
import NewForm from './new_form.jsx'
import axios from 'axios';


class AddGroup extends Component {

  constructor(){
    super();
    this.state = {
      partners: null,
      dataFetch: false
      
    }
    this.fetchData = this.fetchData.bind(this);
  }




 fetchData (){
    let that = this
   axios.get('/api/transactions/new',{
    })
   .then(function(response){
    console.log(response);
    that.setState({
          dataFetch: true,
          partners: response.data.partners
    })
   })
 }



 componentWillMount(){
    this.fetchData();
  }



  render() {
    const dataFetch = this.state.dataFetch;
    const partners = this.state.partners;
    const button = dataFetch ? (
      <NewForm partners={partners}/>
    ) : (
     ""
    );  
    
    return (
       <div>
       {button}
       </div>
       
    );
  }

}


export default AddGroup;