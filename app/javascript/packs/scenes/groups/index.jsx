import React, { Component } from 'react';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import axios from 'axios';

const styles = theme => ({
  mainCont: {
   width: '650px',
   margin: '0 auto'
  },

  eachGroup: {
    fontSize: "15px",
    fontWeight: "bold"
  }

});


class GroupIndex extends Component {

  constructor(){
    super();
    this.state = {
      groups: null,
      dataFetch: false
      
    }
    this.fetchData = this.fetchData.bind(this);
  }






 fetchData (){
   let that = this
   axios.get('/api/groups',{
    })
   .then(function(response){
    console.log(response);
    that.setState({
          dataFetch: true,
          groups: response.data
    })
   })
 }



 componentWillMount(){
    this.fetchData();
  }


  

   render() {

      const {dataFetch, groups} = this.state;
      const { classes } = this.props;
      const display_data = dataFetch ? (

          this.state.groups.map((i) => {
            return (
              <div className={classes.eachGroup}>
            {i.name} 
             </div>
            )
              })
      ) : (
       <p>No Groups to display</p>
      );  
      
      return (
         <div className={classes.mainCont}>
          <h2> My Groups </h2>
            {display_data}
         </div>
         
      );
   }
}


GroupIndex.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(GroupIndex);