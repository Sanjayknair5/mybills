import React, { Component } from 'react';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import axios from 'axios';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';


const styles = theme => ({
 
  mainCont: {
   width: '650px',
   margin: '0 auto'
  },

  statusCont: {
    width: '200px',
    margin: '0 auto',
    fontSize: '14px',
    fontWeight: 'bold' 
  },

  navLink: {
    color: '#fff',
    margin: '8px',
    display: 'block',
    padding: '11px',
    fontSize: '15px',
    background: '#3f51b5',
    float: 'left',
    textDecoration: 'none',
    fontWeight: 'bold',
  }
});


class Home extends Component {

  constructor(){
    super();
    this.state = {
      payable: null,
      receivable: null,
      balance: null,
      statusMsg: null,
      dataFetch: false
    }
    this.fetchData = this.fetchData.bind(this);
  }






 fetchData (){
    let that = this
   axios.get('/api/users/fetch_basic_data',{
    })
   .then(function(response){
    console.log(response);
    that.setState({
          dataFetch: true,
          payable: response.data.payable,
          receivable: response.data.receivable,
          balance: response.data.balance,
          statusMsg: response.data.status_msg
    })
   })
 }



 componentWillMount(){
    this.fetchData();
  }

   render() {
      const {dataFetch, payable, receivable, balance, statusMsg } = this.state;
      const { classes } = this.props;
      const d_data = dataFetch ? (
            
              <div>
               Payable: {payable} <br/>
             
               Receivable: {receivable} <br/>
             
               {statusMsg} {balance} 
             </div>
            
              
      ) : (
       ""
      );  
      
      return (

        <div className={classes.mainCont}>
         <div className={classes.statusCont}>
            {d_data}
         </div>

         <div>
         
       <Link to="/add_transaction" className={classes.navLink}>New Transaction</Link>
       <Link to="/groups" className={classes.navLink} >Groups</Link>
       <Link to="/add_group" className={classes.navLink} > Add Group</Link>
       <Link to="/transactions" className={classes.navLink} > Transactions</Link>  
        </div>
        </div>
         
      );
   }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(Home);