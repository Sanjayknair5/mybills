import React, { Component } from 'react';
import NewForm from './new_form.jsx'
import axios from 'axios';


class AddTransaction extends Component {

  constructor(){
    super();
    this.state = {
      partners: null,
      groups: null,
      dataFetch: false
      
    }
    this.fetchData = this.fetchData.bind(this);
  }




 fetchData (){
    let that = this
   axios.get('/api/transactions/new',{
    })
   .then(function(response){
    console.log(response);
    that.setState({
          dataFetch: true,
          partners: response.data.partners,
          groups: response.data.groups
    })
   })
 }



 componentWillMount(){
    this.fetchData();
  }


  






   render() {

      const dataFetch = this.state.dataFetch;
      const partners = this.state.partners;
      const groups = this.state.groups;
      const button = dataFetch ? (
        <NewForm partners={partners} groups={groups}/>
      ) : (
       ""
      );  
      
      return (
         <div>
         {button}
         </div>
         
      );
   }
}


export default AddTransaction;