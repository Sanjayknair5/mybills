import React, { Component } from 'react';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import axios from 'axios';

const styles = theme => ({
  mainCont: {
   width: '650px',
   margin: '0 auto'
  },

  eachGroup: {
    fontSize: "15px",
    fontWeight: "bold"
  }

});


class TransactionIndex extends Component {

  constructor(){
    super();
    this.state = {
      transactions: null,
      dataFetch: false
      
    }
    this.fetchData = this.fetchData.bind(this);
  }






 fetchData (){
   let that = this
   axios.get('/api/transactions',{
    })
   .then(function(response){
    console.log(response);
    that.setState({
          dataFetch: true,
          transactions: response.data.transactions
    })
   })
 }



 componentWillMount(){
    this.fetchData();
  }


  

   render() {

      const {dataFetch, transactions} = this.state;
      const { classes } = this.props;
      const display_data = dataFetch ? (

          this.state.transactions.map((i) => {
            return (
              <div className={classes.eachGroup}>
               {i}
             </div>
            )
              })
      ) : (
       <p>No Transactions Present</p>
      );  
      
      return (
         <div className={classes.mainCont}>
          <h2> My Recent Transactions </h2>
            {display_data}
         </div>
         
      );
   }
}


TransactionIndex.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(TransactionIndex);