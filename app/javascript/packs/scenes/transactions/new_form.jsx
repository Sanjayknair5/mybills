import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Switch from 'material-ui/Switch';
import axios from 'axios';
import { BrowserRouter as Router, Route, Link , Redirect} from 'react-router-dom';
import Input, { InputLabel, InputAdornment} from 'material-ui/Input';
import { FormControl, FormHelperText } from 'material-ui/Form';
import Select from 'material-ui/Select';
import TextField from 'material-ui/TextField';
import Icon from 'material-ui/Icon';
import Save from 'material-ui-icons/Save';
import Button from 'material-ui/Button';
import classNames from 'classnames';


const styles = theme => ({
  rootForm: {
    width: '600px',
    margin: '0 auto',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },


   button: {
    margin: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
 
  iconSmall: {
    fontSize: 20,
  },

  eachFormComp:{
    margin: '0 auto',
    float: 'none',
  }
});


class NewForm extends React.Component {
  constructor(props){
    super();
    this.state = {
      partners: props.partners,
      groups: props.groups,
      recipient_id: '',
      amount: '',
      name: '',
      mode: 'paid_by_me_and_split_equally',
      fireRedirect: false
      
    }
    this.handleClick = this.handleClick.bind(this);
  };


  componentWillMount(){
    console.log(this.state.partners.length)
  };


  handleClick = (event) => {
   var self = this; 
   if (this.state.recipient_id &&  this.state.amount && this.state.name){
       var apiBaseUrl = "/api/";
       var payload={
       "recipient_id":this.state.recipient_id,
       "amount":this.state.amount,
       "name": this.state.name,
       "mode": this.state.mode
       }
       axios.post(apiBaseUrl+'transactions', {"transaction": payload})
       .then(function (response) {
       console.log(response);
       if(response.data.status == "success"){
       console.log("Transaction successfull");
       self.setState({ fireRedirect: true })
       }else if(response.status == 401){
       console.log("Invalid input");
       }
       else{
       console.log("Error Occured");
       }
       })
       .catch(function (error) {
         console.log(error);
       });
    }else{
    alert("Please will all fields");
    }
  };


  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };


  render() {
    
    const { classes } = this.props;
    const { from } =  '/add_transaction'
    const { partners, fireRedirect, groups} = this.state

    let options = [];
    for (let i=0; i < partners.length ; i += 1) { options.push([partners[i].name,partners[i].id]); }
    return (
      <div className={classes.rootForm}>
         <h4>Add New Transaction</h4>
         <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="recipient_id"> Recipient</InputLabel>
          <Select
            native
            value={this.state.recipient_id}
            onChange={this.handleChange('recipient_id')}
            inputProps={{
              name: 'recipient_id',
              id: 'recipient_id',
            }}
          >
            <option value="" />
            <optgroup label="Friends">
            {partners.map((i) => {
              return (<option  value={i.id+"_user"}> {i.name} </option>)
            })}
            </optgroup>

            <optgroup label="Groups">
            {groups.map((i) => {
              return (<option  value={i.id+"_group"}> {i.name} </option>)
            })}
            </optgroup>
          </Select>
          </FormControl>

          <FormControl className={classes.formControl}>
          <InputLabel htmlFor="mode"> Mode</InputLabel>
          <Select
            native
            value={this.state.mode}
            onChange={this.handleChange('mode')}
            inputProps={{
              name: 'mode',
              id: 'mode',
            }}
          >
            <option  value={"paid_by_me_and_split_equally"}> {"Payed by me and split equally"} </option>
          </Select>
          </FormControl>

           <FormControl className={classes.formControl}>
            <InputLabel htmlFor="transaction-name">Name</InputLabel>
            <Input
              id="transaction-name"
              value={this.state.name}
              onChange={this.handleChange('name')}
            />
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="transaction-amount">Amount</InputLabel>
            <Input
              id="transaction-amount"
              value={this.state.amount}
              onChange={this.handleChange('amount')}
              startAdornment={<InputAdornment position="start">$</InputAdornment>}
            />
         </FormControl>

        <Button className={classes.button} variant="raised" size="small" onClick={(event) => this.handleClick(event)} >
        <Save className={classNames(classes.leftIcon, classes.iconSmall)} />
        Save
      </Button>
        
      </form>

        {fireRedirect && (
          <Redirect to={from || '/'}/>
        )}
      </div>
    );
  }
}

NewForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NewForm);
