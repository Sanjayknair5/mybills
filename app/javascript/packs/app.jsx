import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link , Redirect} from 'react-router-dom';
import CssBaseline from 'material-ui/CssBaseline';
import MenuAppBar from './scenes/layouts/nav_bar.jsx';
import Home from './scenes/home/index.jsx'
import Preload from './scenes/home/preload.jsx'
import Login from './scenes/users/login.jsx'
import AddTransaction from './scenes/transactions/add_transaction.jsx'
import AddGroup from './scenes/groups/add_group.jsx'
import GroupIndex from './scenes/groups/index.jsx'
import TransactionIndex from './scenes/transactions/index.jsx'
import axios from 'axios';


class App extends Component {

  constructor(){
    super();
    this.state = {
      currentUser: null,
      userCheck: false,
      dataFetch: false,
      redirect_to: null,
      partners: null

    }
    this.updateCurrentUser = this.updateCurrentUser.bind(this);
   
  }


 addTodoItem (todoItem) {
    alert("executed");
    this.state.todos.push(todoItem);
    this.setState({todos: this.state.todos});
 }



 componentWillMount(){
     console.log('GrandChild did mount.');
     axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
      };

    let that = this
    axios.get('/api/users/get_user',{
    })
    .then(function(response){
      console.log(response);
      if(response.data.status){
        that.setState({
          currentUser: response.data.name,
          userCheck: true
        })
      } else {
        that.setState({
          currentUser: null,
          userCheck: true
        })
      }
     that.forceUpdate();
    })
    .catch(function(error){
      console.log(error);
    })
  }

updateCurrentUser(name) {
    this.setState({
      currentUser: name
    })
  }

isLoggedin(t=null){
  console.log(t);
  if(this.state.currentUser != null){
    return true;
  }else{
    return false ;
  }
}



   render() {
      const userCheck = this.state.userCheck;
      const loggedIn = this.state.currentUser
      const dataFetch = this.state.dataFetch
      const partners = this.state.partners

      



      return (
         <Router>
             <div>
                <CssBaseline />
                    <Switch> 
                      
                      <div>  
                      {userCheck ? (
                        loggedIn ? (  
                          <MenuAppBar auth={true}></MenuAppBar>
                        ):(
                          <MenuAppBar auth={false}></MenuAppBar>))
                        :("")
                        }


                        <Route exact path='/Login' component={Login} />

                        <Route exact path='/groups' component={GroupIndex} />

                        <Route exact path='/add_transaction' component={AddTransaction} />

                         <Route exact path='/add_group' component={AddGroup} />

                         <Route exact path='/transactions' component={TransactionIndex} />

                       


                        <Route exact path="/" render={() => (
                        userCheck ? (
                        loggedIn ? ( <Home/> ) :  (<Redirect to="/login"/>)
                          ) : (
                          <Preload/>
                          )
                        )}
                        />
                       </div>
                        
               
                   </Switch>

                    
                  

                   
            </div>
         </Router>
      );
   }
}


export default App;