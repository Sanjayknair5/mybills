// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.
import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Switch, Route, Link , Redirect} from 'react-router-dom';
import CssBaseline from 'material-ui/CssBaseline';
import MenuAppBar from './scenes/layouts/nav_bar.jsx';
import Home from './scenes/home/index.jsx'
import Preload from './scenes/home/preload.jsx'
import Login from './scenes/users/login.jsx'
import axios from 'axios';


class App extends Component {

  constructor(){
    super();
    this.state = {
      currentUser: null,
      userCheck: false,
      redirect_to: null
    }
    this.updateCurrentUser = this.updateCurrentUser.bind(this);
  }


 addTodoItem (todoItem) {
    alert("executed");
    this.state.todos.push(todoItem);
    this.setState({todos: this.state.todos});
 }



 componentWillMount(){
     console.log('GrandChild did mount.');
     axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
      };

    let that = this
    axios.get('/api/users/get_user',{
    })
    .then(function(response){
      console.log(response);
      if(response.data.status){
        console.log("kayari");
        that.setState({
          currentUser: response.data.name,
          userCheck: true
        })
      } else {
        console.log("purath");
        that.setState({
          currentUser: null,
          userCheck: true
        })
      }
     that.forceUpdate();
    })
    .catch(function(error){
      console.log(error);
    })
  }

updateCurrentUser(name) {
    this.setState({
      currentUser: name
    })
  }

isLoggedin(t=null){
  console.log(t);
  if(this.state.currentUser != null){
    return true;
  }else{
    return false ;
  }
}

userFetched(){

  console.log(this.state.userCheck);
  if(this.state.userCheck){
    return true;
  }else{
    return false ;
  }
}



   render() {
      const userCheck = this.state.userCheck;
      const loggedIn = this.isLoggedin();

      return (
         <Router>
             <div>
                <CssBaseline />
                    {loggedIn ? (
                    <MenuAppBar auth={true}></MenuAppBar>
                    ) : (
                     ""
                    )}
               <hr />
               
               <Switch>
                  <Route exact path="/" render={() => (
                    this.userFetched("/") ? (
                      this.isLoggedin() ? ( <Home/> ) :  (<Redirect to="/login" addTodoItem={this.addTodoItem}/>)
                      
                    ) : (
                      <Preload/>
                    )
                  )}/>
                  <Route exact path='/Login' component={Login} />
               </Switch>
            </div>
         </Router>
      );
   }
}



document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <App />,
    document.body.appendChild(document.createElement('div')),
  )
})











