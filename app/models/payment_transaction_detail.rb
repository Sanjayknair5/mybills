class PaymentTransactionDetail < ApplicationRecord
  belongs_to :payment_transaction
  belongs_to :user
  validates :payed, :share, numericality: true , presence: true 


  def balance

   self.payed - self.share
  end  
end
