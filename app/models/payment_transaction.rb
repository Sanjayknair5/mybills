class PaymentTransaction < ApplicationRecord
  belongs_to :user
  belongs_to :recipient, :polymorphic => true
  has_many :payment_transaction_details
  validates :user_id, :recipient, :mode, :name, presence: true
  validates :amount, numericality: true , presence: true
  accepts_nested_attributes_for :payment_transaction_details
  validates_associated :payment_transaction_details

  attr_accessor :pay_hash


  def self.modes
    ["paid_by_me_and_split_equally"]
  end


  def self.check_and_create(params,user)
    members = []
    t = user.payment_transactions.new(amount: params[:amount],name:params[:name],mode: params[:mode])
    t.pay_hash = {negative: [], positive: []}
    recipient_split = params[:recipient_id].split("_")
    case recipient_split.last
    when "user"
      recp = User.find(recipient_split[0])
      t.recipient = recp
      members << recp
      members << user
    when "group"
      group = Group.where("id = #{recipient_split[0]}").includes(:users).limit(1).first
      t.recipient = group
      members = group.users
    end

    case t.mode
    when "paid_by_me_and_split_equally"
      t.process_paid_by_me_and_split_equally(members)
    end
    if t.save
      t.update_individual_ledger(members)
      t.update_master_ledger
    end
    return t
  end


  def process_paid_by_me_and_split_equally(members)
    eq_share  = (self.amount.to_f/members.count).round(1)
    members.each do |u|
      s = self.payment_transaction_details.build(user_id: u.id, share: eq_share)
      s.payed = (u.id == self.user_id ? self.amount : 0)
      balance = s.balance
      if balance > 0
        self.pay_hash[:positive] << [u.id,s.balance,0]
      elsif balance < 0
        self.pay_hash[:negative] << [u.id,s.balance,0]
      end
    end
  end



  def update_individual_ledger(members)
    ledgers = FriendLedger.where(["(u1_id = #{self.user_id} AND u2_id IN (?)) OR (u2_id = #{self.user_id} AND u1_id IN (?))",members.collect(&:id),members.collect(&:id)])
    pay_hash[:positive].sort.reverse!
    pay_hash[:negative].sort!
    pay_hash[:negative].each do |payer|
      pay_hash[:positive].each do |receiver|
        if receiver[1].abs > receiver[2]
          can_take =  receiver[1].abs - receiver[2]
          can_pay = payer[1].abs - payer[2]
          payed = 0
          if can_take >= can_pay
            payed = can_pay
          else
            payed = can_take
          end
          receiver[2] += payed
          payer[2] += payed

          ledger = ledgers.select{|x|(x.u1_id == receiver[0] and x.u2_id == payer[0]) or (x.u2_id == receiver[0] and x.u1_id == payer[0])}.first
          ledger ||= FriendLedger.new(u1_id: receiver[0], u2_id: payer[0], balance: 0)

          if ledger.u1_id == receiver[0]
            ledger.balance += payed
          else
            ledger.balance -= payed
          end
          ledger.save
        end
        if payer[1].abs <= payer[2]
          break
        end
      end
    end
  end

  def update_master_ledger
    details = payment_transaction_details
    m_ledgers = MasterLedger.where(user_id: details.collect(&:user_id))
    details.each do |trns|
      m_ledger = m_ledgers.select{|x|x.user_id == trns.user_id}.first
      m_ledger ||= MasterLedger.new(user_id: trns.user_id,payable: 0, receivable: 0)
      m_ledger.payable += trns.share
      m_ledger.receivable += trns.payed
      m_ledger.save
    end
  end

end
